module CSE151KNeighbors

# package code goes here
include("stats.jl")
include("dataProcessing.jl")
include("KNeighbors.jl")
include("ConfusionMatrix.jl")

end # module
