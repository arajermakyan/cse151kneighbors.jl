# CSE151KNeighbors


## Dependencies:
This package depends on unregistered packages:
```julia
  #DataParser
  Pkg.clone("https://bitbucket.org/arajermakyan/cse151dataparser.jl")
  #Unit Test Framework
  Pkg.clone("https://github.com/ajermaky/RunTests.jl")
```

## Install
```julia
Pkg.clone("https://bitbucket.org/arajermakyan/cse151dataparser.jl")
Pkg.resolve()
```


##Use:

To use this module:
```julia
  using CSE151KNeighbors
```

##Test:

To Run Unit Tests:
```julia
  Pkg.test("CSE151KNeighbors")
```

To run custom KNeighbors test that regenerates confusion matrices and graph:
```julia
  cd("~/.julia/v0.4/CSE151KNeighbors")
  include("test/high_level_test/KNeighbors.jl")
```
##Unintsall
```julia
  Pkg.rm("CSE151KNeighbors")
```
