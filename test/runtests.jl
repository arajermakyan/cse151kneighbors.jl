using RunTests
using Base.Test
using CSE151KNeighbors
module RegressionTest

end
RunTests.set_show_progress(false)

println("Running Tests")
println("...")

tests = ["unit_tests"]

for t in tests
  run_tests(t)
end

println("...")
println("Tests Done")
