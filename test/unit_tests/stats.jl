using RunTests
using Base.Test

@testmodule Stats begin
using CSE151KNeighbors

  function test_meanColumn()
    A = [1 2 3 4;5 6 7 8]
    @test CSE151KNeighbors.ExtraStats.meanColumn(A)==[3.0 4.0 5.0 6.0]
    # @test 1==0
  end

  function test_stdColumn()
    A = [1 2 3 4;5 6 7 8]
    @test CSE151KNeighbors.ExtraStats.stdColumn(A)==[2.8284271247461903 2.8284271247461903 2.8284271247461903 2.8284271247461903]
    # @test 1==0
  end

  function test_getMinIndices()
    A=[1;2;-1;5;3;2;9;-3; 10]
    @test CSE151KNeighbors.ExtraStats.getMinIndices(A)==[8]
    @test CSE151KNeighbors.ExtraStats.getMinIndices(A,2)==[8, 3]
    @test CSE151KNeighbors.ExtraStats.getMinIndices(A,3)==[8, 3, 1]
    @test CSE151KNeighbors.ExtraStats.getMinIndices(A,4)==[8, 3, 1, 2]
  end

  function test_getMajority()
    A=[1;2;-1;5;3;2;9;-3;10]
    @test CSE151KNeighbors.ExtraStats.getMajority(A)==2
    A=[1;2;1;5;3;2;9;-3;10]
    maj=CSE151KNeighbors.ExtraStats.getMajority(A)
    @test maj==1 || maj==2
    A=[1]
    @test CSE151KNeighbors.ExtraStats.getMajority(A)==1


  end
end
